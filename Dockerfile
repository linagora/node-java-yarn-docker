FROM node:10.15.2-stretch

MAINTAINER Petals / Linagora

 # Note: no need to clean after, it is done automatically by apt
RUN apt-get -q update && apt-get -y -q install \
      chromium=73.0.3683.75-1~deb9u1 \
      openjdk-8-jre-headless \
      # The next dependencies are needed by Cypress, see https://docs.cypress.io/guides/guides/continuous-integration.html#Dependencies
      xvfb libgtk2.0-0 libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 \
    && npm set -g progress=false

RUN  ln -fs /usr/bin/chromium /usr/bin/chromium-browser
